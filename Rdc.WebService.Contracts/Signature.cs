﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Rdc.WebService.Contracts
{
    [DataContract]
    public class Signature
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long Length { get; set; }
    }
}
