﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Rdc.WebService.Contracts;

namespace Rdc.WebService
{    
    [ServiceContract]
    public interface IRdcService
    {
        [OperationContract]        
        SignatureManifest GetSignatureManifest(string fileName);

        [OperationContract]
        byte[] GetSignatureContent(string name, int offset, int length);

        [OperationContract]
        byte[] TransferDataBlock(string fileName, int offset, int length);
    }
}
