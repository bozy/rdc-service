﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Rdc.WebService.Contracts
{
    [DataContract]
    public class SignatureManifest
    {
        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public IList<Signature> Signatures { get; set; }

        [DataMember]
        public long FileLength { get; set; }
    }
}
