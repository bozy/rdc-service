﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rdc.Wrapper.Synchronization.Client;


namespace Rdc.Wrapper.Synchronization
{
    public class Synchronizer
    {

        private readonly IRdcService _rdcServiceClient;
        private const int BlockSize = 2 * 4096;
        private ISignatureRepository _signatureRepository;

        public Synchronizer(ISignatureRepository signatureRepository)
        {
            _rdcServiceClient = new RdcServiceClient();
            _signatureRepository = signatureRepository;
        }

        public void Process(string sourcePath, string seedPath, string outputPath)
        {
            var sourceSignatures = CopySourceSignatures(sourcePath);
            var seedSignatures = GenerateSeedSignatures(seedPath);
            var pairs = seedSignatures.Zip(sourceSignatures, (seed, source) => new Tuple<SignatureInfo, SignatureInfo>(seed, source));
            // the last pair is for source/seed file synchronization the rest is for sig file synchronization. Number of pairs depends on
            // recursion level. Currently all sigs are pulled without synchronization.
            var needList = GenerateNeedList(pairs.Reverse().Take(1));
            ParseNeedList(sourcePath, seedPath, outputPath, needList);
        }

        private void ParseNeedList(string sourcePath, string seedPath, string outputPath, IEnumerable<RdcNeed> needList)
        {
            using (FileStream seedFile = File.OpenRead(seedPath), outputFile = File.Create(outputPath))
            {
                foreach (var item in needList)
                {
                    byte[] data;
                    switch (item.BlockType)
                    {
                        case RdcNeedType.Source:
                            data = _rdcServiceClient.TransferDataBlock(sourcePath, (int)item.FileOffset,
                                                                       (int)item.BlockLength);
                            break;
                        case RdcNeedType.Seed:
                            data = new byte[item.BlockLength];
                            seedFile.Seek((int)item.FileOffset, SeekOrigin.Begin);
                            seedFile.Read(data, 0, (int)item.BlockLength);
                            break;
                        default:
                            data = null;
                            break;
                    }
                    if (data != null)
                    {
                        outputFile.Write(data, 0, (int)item.BlockLength);
                    }
                }
            }
        }

        internal IList<RdcNeed> GenerateNeedList(IEnumerable<Tuple<SignatureInfo, SignatureInfo>> seedAndSources)
        {
            var result = new List<RdcNeed>();
            var needListGenerator = new NeedListGenerator(_signatureRepository, _signatureRepository);
            foreach (var item in seedAndSources)
            {
                result = result.Concat(needListGenerator.CreateNeedsList(item.Item1, item.Item2)).ToList();
            }            
            return result;
        }

        private IList<SignatureInfo> GenerateSeedSignatures(string sourceFileName)
        {
            IList<SignatureInfo> result;
            using (var sourceFile = File.OpenRead(sourceFileName))
            {
                var sigGenerator = new SigGenerator(_signatureRepository);
                result = sigGenerator.GenerateSignatures(sourceFile);
            }
            return result;
        }

        internal IList<SignatureInfo> CopySourceSignatures(string sourceFileName)
        {
            var result = new List<SignatureInfo>();
            var signatureManifest = _rdcServiceClient.GetSignatureManifest(sourceFileName);
            foreach (var signature in signatureManifest.Signatures)
            {
                var sourceSignatureInfo = new SignatureInfo();
                using (var sourceStream = _signatureRepository.CreateContent(sourceSignatureInfo.Name))
                {
                    for (var i = 0; i < signature.Length; i += BlockSize)
                    {
                        var readBytes = Math.Min((int)(signature.Length - i), BlockSize);
                        var data = _rdcServiceClient.GetSignatureContent(signature.Name, i, readBytes);
                        sourceStream.Write(data, 0, data.Length);
                    }
                }
                result.Add(sourceSignatureInfo);
            }
            return result;
        }        
    }
}