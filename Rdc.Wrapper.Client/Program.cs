﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Rdc.Wrapper.Synchronization;

namespace Rdc.Wrapper.Client
{
    class Program
    {
        static void Main(string[] args)
        {            
            if (args.Length != 3)
            {
                Console.WriteLine(@"Usage: Rdc.rapper.Client.exe sourceFile seedFile outputFile ");
                Console.WriteLine(@"Ex: Rdc.rapper.Client.exe c:\temp\source.txt .\seed.txt .\result.txt ");
                return;
            }


            var synchronizer = new Synchronizer(new SimpleSignatureRepository());
            synchronizer.Process(args[0], args[1], args[2]);
        }
    }
}
